﻿using System;

namespace Week4
{

    class Program
    {

        static void Main(string[] args)
        {

            DLL dll = new DLL();

            //adaug valori in lista

            dll.push(1);
            dll.push(2);
            dll.push(3);
            dll.push(5);
            dll.push(7);
            dll.push(90);
            dll.push(45);
            dll.push(21);
            dll.push(15);
            dll.push(18);

            //afisez

            Console.Write("DLL creat este: ");
            dll.printlist(dll.head);
            Console.WriteLine(" ");

            //sterg valoarea 45

            dll.deleteNode(dll.head, dll.getNode(3));

            //afisez lista dupa stergerea lui 3

            Console.Write("DLL dupa stergerea lui 45 este: ");
            dll.printlist(dll.head);
            Console.WriteLine(" ");

            //caut elementul 21

            DLL.searchList(dll.head, 21);

            //sort

            Console.WriteLine(" ");

            dll.head = DLL.bubbleSort(dll.head);

            //afisez dupa sortare

            Console.Write("DLL sortat este: ");
            dll.printlist(dll.head);
            Console.WriteLine(" ");

        }

    }

}
