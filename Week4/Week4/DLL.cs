﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week4
{
    class DLL //clasa lista
    {

        public DLL()
        {

        }

        public Node head;
        
 
        public void push(int new_data) // adaug nod la inceput
        {

            Node new_Node = new Node(new_data); //creez un nou nod, ii atribui valoarea

            new_Node.next = head;
            new_Node.prev = null;

            if (head != null)
            {

                head.prev = new_Node;

            }

            head = new_Node;

        }

        public void printlist(Node node) //printeaza lista
        {

            Node last = null;

            while (node != null)
            {

                Console.Write(node.data + " ");
                last = node;
                node = node.next;

            }

            Console.WriteLine();

        }

        public Node getNode(int index)
        {
            if(index < 0)
            {
                return null;
            }

            Node node = head;
            while(index != 0 && node != null)
            {
                index--;
                node = node.next;
            }

            return node;
        }

        public void deleteNode(Node head_ref, Node del)

        // Sterg un nod din lista 
        // head_ref = nodul head 
        // del = data din nodul pe care vrem sa-l stergem 

        {

            if (head == null || del == null)
            {

                return;

            }

            if (head == del)
            {

                head = del.next;

            }

            if (del.next != null)
            {

                del.next.prev = del.prev;

            }

            if (del.prev != null)
            {

                del.prev.next = del.next;

            }

            return;

        }

        public static Node bubbleSort(Node head) //sortez lista folosind metoda bubble sort
        {

            int swapped;
            Node ptr1;
            Node lptr = null;

            if (head == null)
            {

                return null;

            }

            do
            {

                swapped = 0;
                ptr1 = head;

                while (ptr1.next != lptr)
                {

                    if (ptr1.data > ptr1.next.data)
                    {

                        int t = ptr1.data;
                        ptr1.data = ptr1.next.data;
                        ptr1.next.data = t;
                        swapped = 1;

                    }
                    ptr1 = ptr1.next;

                }
                lptr = ptr1;

            }

            while (swapped != 0);

            return head;

        }

        public static int searchList(Node head, int search) //caut in lista
        {
  
            Node temp = head;

            int count = 0, flag = 0;

            if (temp == null)
            {

                return -1;

            }

            else
            {

                while (temp.next != head)
                {
                   
                    count++;

                    if (temp.data == search)
                    {

                        flag = 1;
                        count--;
                        break;

                    }
 
                    temp = temp.next;

                }
 
                if (temp.data == search)
                {

                    count++;
                    flag = 1;

                }

                if (flag == 1)
                {

                    Console.WriteLine(search + " a fost gasit pe pozitia " + count);

                }

                else
                {

                    Console.WriteLine("\n" + search + " nu a fost gasit");

                }

            }

            return -1;
        }

    }

}