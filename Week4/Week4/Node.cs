﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week4
{
    class Node // clasa pentru nodurile din lista
    {

        public int data;
        public Node prev; //null
        public Node next; //null

        public Node (int d) //constructor
        {

            data = d; 

        }

    }
}
