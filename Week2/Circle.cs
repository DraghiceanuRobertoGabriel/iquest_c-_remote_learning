﻿using System;
using System.Collections.Generic;
using System.Text;

namespace week2_homework
{
    struct Circle : IGeometricalFigure
    {
        private int radius;
        private int x;
        private int y;
        private int points;

        //Constructor
        public Circle(int radius, int x, int y)
        {
            this.radius = radius;
            this.x = x;
            this.y = y;
            this.points = 0;
        }

        //Compute area and perimeter
        public int computeArea()
        {
            return (int)(radius * radius * Math.PI);
        }
        public int computePerimeter()
        {
            return (int)(radius * 2 * Math.PI);
        }

        //Get type - used for output
        public string getType()
        {
            return "Circle";
        }

        //Edit a figure
        public void editFigure(int a, int b)
        {
            this.radius = a;
            Console.Out.WriteLine("A " + getType() + " was edited with the new radious " + a);
        }

        //Getters and setters
        public int Radius
        {
            get
            {
                return radius;
            }
            set
            {
                radius = value;
            }
        }

        public int X
        {
            get
            {
                return x;
            }
            set
            {
                x = value;
            }
        }
        public int Y
        {
            get
            {
                return y;
            }
            set
            {
                y = value;
            }
        }

        public int Points
        {
            get
            {
                return points;
            }
        }
    }
}
