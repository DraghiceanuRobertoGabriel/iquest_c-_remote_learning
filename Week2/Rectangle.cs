﻿using System;

namespace week2_homework
{
    struct Rectangle : IGeometricalFigure
    {
        private int length;
        private int width;
        private int x;
        private int y;
        private int points;

        //Constructor
        public Rectangle(int length, int width, int x, int y)
        {
            this.length = length;
            this.width = width;
            this.x = x;
            this.y = y;
            points = 4;
        }

        //Compute area and perimeter
        public int computeArea()
        {
            return length * width;
        }
        public int computePerimeter()
        {
            return 2 * (length + width);
        }

        //Get type - used for output
        public string getType()
        {
            return "Rectangle";
        }

        //Edit a figure
        public void editFigure(int a, int b)
        {
            this.length = a;
            this.width = b;
            Console.Out.WriteLine("A " + getType() + " was edited with the new length " + a+" and the new width "+b);
        }

        //Getters and setters
        public int Length
        {
            get
            {
                return length;
            }
            set
            {
                length = value;
            }
        }
        public int Width
        {
            get
            {
                return width;
            }
            set
            {
                width = value;
            }
        }
        public int X
        {
            get
            {
                return x;
            }
            set
            {
                x = value;
            }
        }
        public int Y
        {
            get
            {
                return y;
            }
            set
            {
                y = value;
            }
        }

        public int Points
        {
            get
            {
                return points;
            }
        }
    }
}
