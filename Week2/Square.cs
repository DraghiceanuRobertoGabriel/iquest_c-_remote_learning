﻿using System;
using System.Collections.Generic;
using System.Text;

namespace week2_homework
{
    struct Square : IGeometricalFigure
    {
        private int length;
        private int x;
        private int y;
        private int points;

        //Constructor
        public Square(int length, int x, int y)
        {
            this.length = length;
            this.x = x;
            this.y = y;
            this.points = 4;
        }

        //Compute area and perimeter
        public int computeArea()
        {
            return length * length;
        }
        public int computePerimeter()
        {
            return length*4;
        }

        //Get type - used for output
        public string getType()
        {
            return "Square";
        }

        //Edit a figure
        public void editFigure(int a, int b)
        {
            this.length = a;
            Console.Out.WriteLine("A " + getType() + " was edited with the new length " + a);
        }

        //Getters and setters
        public int Length
        {
            get
            {
                return length;
            }
            set
            {
                length = value;
            }
        }

        public int X
        {
            get
            {
                return x;
            }
            set
            {
                x = value;
            }
        }
        public int Y
        {
            get
            {
                return y;
            }
            set
            {
                y = value;
            }
        }

        public int Points
        {
            get 
            {
                return points;
            }
        }
    }
}
