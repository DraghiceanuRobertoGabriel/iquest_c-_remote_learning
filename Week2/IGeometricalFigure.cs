﻿using System;
using System.Collections.Generic;
using System.Text;

namespace week2_homework
{
    interface IGeometricalFigure
    {
        int computeArea();
        int computePerimeter();

        void editFigure(int a, int b);

        string getType();

        int X { get; set; }

        int Y { get; set; }

        int Points { get; }
    }
}
