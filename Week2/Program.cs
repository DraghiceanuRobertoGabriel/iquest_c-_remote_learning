﻿using System;
using System.Collections;

namespace week2_homework
{
    //I know this main doesnt look very nice, i did it this way just to test the functionalities.

    class Program
    {
        static void Main(string[] args)
        {
            Plane basePlane = new Plane();

            //Declare the figures
            Rectangle rectangle1 = new Rectangle(10, 20, 10, 2);
            Rectangle rectangle2 = new Rectangle(10, 20, 4, 7);
            Square square1 = new Square(10, 15, 13);
            Square square2 = new Square(10, 7, 1);
            Rhombus rhombus1 = new Rhombus(10, 60, 15, 5);
            Rhombus rhombus2 = new Rhombus(10, 60, 25, 2);
            Circle circle1 = new Circle(10, 16, 4);
            Circle circle2 = new Circle(10, 13, 4);
            Triangle triangle1 = new Triangle(10, 7, 4);
            Triangle triangle2 = new Triangle(10, 3, 4);

            //Add the figures to the plane
            basePlane.addFigure(rectangle1);
            basePlane.addFigure(rectangle2);
            basePlane.addFigure(square1);
            basePlane.addFigure(square2);
            basePlane.addFigure(rhombus1);
            basePlane.addFigure(rhombus2);
            basePlane.addFigure(circle1);
            basePlane.addFigure(circle2);
            basePlane.addFigure(triangle1);
            basePlane.addFigure(triangle2);

            Console.Out.Write("\n");

            //Remove some Figures
            basePlane.removeFigure(circle2);
            basePlane.removeFigure(triangle2);

            Console.Out.Write("\n");

            //Move some figures
            basePlane.moveFigure(rectangle1, 6, 7);
            basePlane.moveFigure(circle1, 6, 7);

            Console.Out.Write("\n");

            //Edit some figures
            basePlane.editFigure(triangle1, 10, 0);
            basePlane.editFigure(rhombus1, 7, 50);

            //Test some functions of the plane
            Console.Out.WriteLine("\nThere are a total of "+basePlane.nrFigures()+" figures in the plane.");
            Console.Out.WriteLine("There are a total of " + basePlane.nrSpecificFigure(typeof(Rectangle)) + " rectangles.");
            Console.Out.WriteLine("There are a total of " + basePlane.nrSpecificFigure(typeof(Square)) + " squares.\n");
            Console.Out.WriteLine("The total area of the figures in the plane is "+basePlane.totalArea());
            Console.Out.WriteLine("The total perimeter of the figures in the plane is " + basePlane.totalPerimeter());
            Console.Out.WriteLine("The total points of the figures in the plane is " + basePlane.totalPoints());

            Console.Out.WriteLine("\n");
        }
    }
}
