﻿using System;
using System.Collections.Generic;
using System.Text;

namespace week2_homework
{
    struct Rhombus : IGeometricalFigure
    {
        private int length;
        private int angle;
        private int x;
        private int y;
        private int points;

        //Constructor
        public Rhombus(int length, int angle, int x, int y)
        {
            this.length = length;
            this.angle = angle;
            this.x = x;
            this.y = y;
            this.points = 4;
        }

        //Compute area and perimeter
        public int computeArea()
        {
            return (int)(length * length * Math.Sin((double)angle));
        }
        public int computePerimeter()
        {
            return length * 4;
        }

        //Get type - used for output
        public string getType()
        {
            return "Rhombus";
        }

        //Edit a figure
        public void editFigure(int a, int b)
        {
            this.length = a;
            this.angle = b;
            Console.Out.WriteLine("A " + getType() + " was edited with the new length " + a + " and the new angle " + b);
        }

        //Getters and setters
        public int Length
        {
            get
            {
                return length;
            }
            set
            {
                length = value;
            }
        }

        public int X
        {
            get
            {
                return x;
            }
            set
            {
                x = value;
            }
        }
        public int Y
        {
            get
            {
                return y;
            }
            set
            {
                y = value;
            }
        }

        public int Points
        {
            get
            {
                return points;
            }
        }
    }
}
