﻿using System;
using System.Collections;

namespace week2_homework
{
    class Plane
    {
        //All figures in the plane
        private ArrayList allFigures = new ArrayList();

        //The constructor of the plane
        public Plane() 
        {
            //EMPTY
        }

        //Add a new figure
        public void addFigure(IGeometricalFigure figure)
        {
            Console.WriteLine("There was a "+figure.getType()+" added at "+figure.X+" and "+figure.Y);
            allFigures.Add(figure);
        }

        //Remove a figure
        public void removeFigure(IGeometricalFigure figure)
        {
            Console.WriteLine("A " + figure.getType() + " was removed from the plane.");
            allFigures.Remove(figure);
        }

        //Move a figure
        public void moveFigure(IGeometricalFigure figure, int x, int y)
        {
            var auxFigure = (IGeometricalFigure)allFigures[allFigures.IndexOf(figure)];
            Console.WriteLine("A " + figure.getType() + " was moved to "+x+" and "+y+".");
            auxFigure.X = x;
            auxFigure.Y = y;
        }

        //Edit a figure
        public void editFigure(IGeometricalFigure figure, int a, int b) {
            var auxFigure = (IGeometricalFigure)allFigures[allFigures.IndexOf(figure)];
            allFigures.Remove(figure);
            auxFigure.editFigure(a, b);
            allFigures.Add(figure);
        }

        //Get the number of a specific figure
        public int nrSpecificFigure(Type type) 
        {
            int nrFigures = 0;
            foreach (IGeometricalFigure figure in allFigures)
            {
                if (figure.GetType() == type)
                {
                    nrFigures += 1;
                }
            }
            return nrFigures;
        }

        //Get the number of all figures in the plane
        public int nrFigures()
        {
            return allFigures.Count;
        }

        //Compute the total area for all figures
        public int totalArea()
        {
            int totalArea = 0;
            foreach (IGeometricalFigure figure in allFigures)
            {
                totalArea += figure.computeArea();
            }
            return totalArea;
        }

        //Compute the total perimeter of the figures in the plane
        public int totalPerimeter()
        {
            int totalPerimeter = 0;
            foreach (IGeometricalFigure figure in allFigures)
            {
                totalPerimeter += figure.computePerimeter();
            }
            return totalPerimeter;
        }

        //Total number of points
        public int totalPoints()
        {
            int totalPoints = 0;
            foreach (IGeometricalFigure figure in allFigures)
            {
                totalPoints += figure.Points;
            }
            return totalPoints;
        }
    }
}
