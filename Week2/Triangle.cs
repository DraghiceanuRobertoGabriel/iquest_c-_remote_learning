﻿using System;
using System.Collections.Generic;
using System.Text;

namespace week2_homework
{
    struct Triangle : IGeometricalFigure
    {
        private int side;
        private int x;
        private int y;
        private int points;

        //Constructor
        public Triangle(int side, int x, int y)
        {
            this.side = side;
            this.x = x;
            this.y = y;
            this.points = 3;
        }

        //Compute area and perimeter
        public int computeArea()
        {
            return (int)(side*side*Math.Sqrt(3)/4);
        }
        public int computePerimeter()
        {
            return side * 3;
        }

        //Get type - used for output
        public string getType()
        {
            return "Triangle";
        }

        //Edit a figure
        public void editFigure(int a, int b)
        {
            this.side = a;
            Console.Out.WriteLine("A " + getType() + " was edited with the new side of "+a);
        }

        //Getters and setters
        public int Side
        {
            get
            {
                return side;
            }
            set
            {
                side = value;
            }
        }

        public int X
        {
            get
            {
                return x;
            }
            set
            {
                x = value;
            }
        }
        public int Y
        {
            get
            {
                return y;
            }
            set
            {
                y = value;
            }
        }

        public int Points
        {
            get
            {
                return points;
            }
        }
    }
}
