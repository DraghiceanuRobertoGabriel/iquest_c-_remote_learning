﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week1
{
    class Cat : Animal
    {
        public Cat(string name)
            : base(name)
        {
        }

        protected override string getSound()
        {
            return "miau";
        }

        protected override string getAction()
        {
            return "jumps";
        }
    }
}