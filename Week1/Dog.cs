﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week1
{
    class Dog : Animal
    {
        public Dog(string name)
            : base(name)
        { 
        }

        protected override string getSound()
        {
            return "woof";
        }

        protected override string getAction()
        {
            return "runs";
        }
    }
}
