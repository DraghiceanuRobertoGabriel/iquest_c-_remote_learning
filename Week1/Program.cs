﻿using System;

namespace Week1
{
    class Program
    {
        static void Main(string[] args)
        {
            Animal[] animals = {
                new Dog("Rex"),
                new Dog("Azorel"),
                new Dog("Iancu"),
                new Cat("Max"),
                new Alpaca("Pablo"),
                new Duck("Ducky"),
            };

            foreach (var animal in animals)
            {
                animal.makeSound();
            }

            Console.WriteLine(" ");

            foreach (var animal in animals)
            {
                animal.makeAction();
            }
        }
    }
}
