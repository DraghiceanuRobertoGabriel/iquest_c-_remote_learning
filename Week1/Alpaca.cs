﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week1
{
    class Alpaca : Animal
    {
        public Alpaca(string name)
            : base(name)
        {
        }

        protected override string getSound()
        {
            return "arrrgh";
        }

        protected override string getAction()
        {
            return "levitates";
        }
    }
}