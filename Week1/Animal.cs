﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week1
{
    abstract class Animal
    {
        private String name;
        public Animal(string name)
        {
            this.name = name;
        }

        public void makeSound()
        {
            Console.WriteLine(this.name + " makes " + getSound());
        }

        public void makeAction()
        {
            Console.WriteLine(this.name + " " + getAction());
        }

        protected abstract string getSound();

        protected abstract string getAction();
    }
}
