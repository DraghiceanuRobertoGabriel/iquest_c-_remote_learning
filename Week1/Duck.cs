﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week1
{
    class Duck : Animal
    {
        public Duck(string name)
            : base(name)
        {
        }

        protected override string getSound()
        {
            return "quac";
        }

        protected override string getAction()
        {
            return "flies";
        }
    }
}