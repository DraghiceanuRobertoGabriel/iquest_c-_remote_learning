﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week5
{
    class Option
    {
        public string message;
        public IScreen nextScreen;

        public Option(string message, IScreen nextScreen)
        {
            this.message = message;
            this.nextScreen = nextScreen;
        }
    }
}
