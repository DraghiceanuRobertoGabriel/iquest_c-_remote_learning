﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week5
{
    class BeginScreen : IScreen
    {
        IScreen beverageScreen;

        public BeginScreen(IScreen beverageScreen)
        {
            this.beverageScreen = beverageScreen;
        }

        public void AddOption(int key, string message, IScreen screen)
        {
        }

        public IScreen HandleButtonPress()
        {
            String userInput = Console.ReadLine();
            int choice = Convert.ToInt32(userInput);

            if(choice == 1)
            {
                return this.beverageScreen;
            } else if(choice == 2){
                return null;
            }

            return this;
        }

        public void Present()
        {
            Console.WriteLine("Hello! How can i help you?");
            Console.WriteLine("1. Order a coffee");
            Console.WriteLine("2. Cancel");
        }
    }
}
