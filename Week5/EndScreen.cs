﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week5
{
    class EndScreen : IScreen
    {
        public IScreen HandleButtonPress()
        {
            return null;
        }

        public void Present()
        {
            Console.WriteLine("Here's your beverage! Please enjoy and come again!");
        }
    }
}
