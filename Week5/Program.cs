﻿using System;

namespace Week5
{
    class Program
    {
        static void AddSizeScreenOptions(Screen sizeScreen, IScreen nextScreen)
        {
            sizeScreen.AddOption("Short", nextScreen);
            sizeScreen.AddOption("Tall", nextScreen);
            sizeScreen.AddOption("Grande", nextScreen);
            sizeScreen.AddOption("Venti", nextScreen);
        }

        static void Main(string[] args)
        {
            const String sizeScreenMessage = "What size do you want ?";

            Screen beverageScreen = new Screen("What beverage do you want ?");
            Screen coffeeSizeScreen = new Screen(sizeScreenMessage);
            Screen teaSizeScreen = new Screen(sizeScreenMessage);
            Screen typeScreen = new Screen("Do you want stronger coffee or a more flavored one ?");

            IScreen beginScreen = new BeginScreen(beverageScreen);
            IScreen endScreen = new EndScreen();

            beverageScreen.AddOption("Caramel Macchiato", coffeeSizeScreen);
            beverageScreen.AddOption("Caffe Latte", coffeeSizeScreen);
            beverageScreen.AddOption("Cappuccino", coffeeSizeScreen);
            beverageScreen.AddOption("Caffe Americano", coffeeSizeScreen);
            beverageScreen.AddOption("White Chocolate Mocha", typeScreen);  //Mereu Venti
            beverageScreen.AddOption("Caffe Mocha", typeScreen); //Mereu Venti
            beverageScreen.AddOption("Chai Tea Latte", teaSizeScreen); //Fara tipul de cafea
            beverageScreen.AddOption("Pumpkin Spice Latte", coffeeSizeScreen);
            beverageScreen.AddOption("Doppio Espresso Macchiato", endScreen); //Mereu Short si Strong
            beverageScreen.AddOption("Espresso Shot", endScreen); //Mereu Short si Strong

            AddSizeScreenOptions(coffeeSizeScreen, typeScreen);
            AddSizeScreenOptions(teaSizeScreen, endScreen);

            typeScreen.AddOption("Strong coffee", endScreen);
            typeScreen.AddOption("Flavored coffee", endScreen);

            IScreen screen = beginScreen;
            while(screen != null)
            {
                Console.Clear();
                screen.Present();
                screen = screen.HandleButtonPress();
            }
        }
    }
}
