﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Week5
{
    interface IScreen
    {
        void Present();

        IScreen HandleButtonPress();
    }
}
