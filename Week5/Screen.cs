﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Week5
{
    class Screen : IScreen
    {
        private string question;
        private List<Option> options;

        public Screen(string question)
        {
            this.options = new List<Option>();

            this.question = question;
        }

        public IScreen HandleButtonPress()
        {
            string userInput = Console.ReadLine();
            int choice = Convert.ToInt32(userInput);

            int index = choice - 1;
            if(index >= 0 && index < this.options.Count)
            {
                return this.options.ElementAt(index).nextScreen;
            }

            return this;
        }

        public void Present()
        {
            String message = this.question + '\n';

            for(int i = 0; i < options.Count; i++)
            {
                message += "" + (i + 1);
                message += ". " + options.ElementAt(i).message + '\n';
            }

            Console.Write(message);
        }

        public void AddOption(string message, IScreen screen)
        {
            options.Add(new Option(message, screen));
        }
    }
}
